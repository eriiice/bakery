package ee.ericerm.pastrybakery;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PastryBakeryApplication{
	public static void main(String[] args) {
		SpringApplication.run(PastryBakeryApplication.class, args);
	}
}
