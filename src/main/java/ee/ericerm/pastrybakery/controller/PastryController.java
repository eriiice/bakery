package ee.ericerm.pastrybakery.controller;

import ee.ericerm.pastrybakery.exception.ResourceNotFoundException;
import ee.ericerm.pastrybakery.model.Ingredient;
import ee.ericerm.pastrybakery.model.Pastry;
import ee.ericerm.pastrybakery.model.PastryIngredient;
import ee.ericerm.pastrybakery.service.IngredientService;
import ee.ericerm.pastrybakery.exception.MapValidationErrorService;
import ee.ericerm.pastrybakery.service.PastryIngredientService;
import ee.ericerm.pastrybakery.service.PastryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Api(value = "Pastry Management")
@RestController
@RequestMapping(value = "/api/pastries", produces = "application/json")
@CrossOrigin(origins = "http://localhost:3000")
@Slf4j
@RequiredArgsConstructor
public class PastryController {
    private final PastryService pastryService;
    private final IngredientService ingredientService;
    private final PastryIngredientService pastryIngredientService;
    private final MapValidationErrorService mapValidationErrorService;

    @ApiOperation(value = "View a list of pastries", response = List.class)
    @GetMapping
    public ResponseEntity<List<Pastry>> findAll() throws ResourceNotFoundException{
        if (pastryService.findAll().isEmpty()){
            throw new ResourceNotFoundException("No pastries found");
        }
        return ResponseEntity.ok(pastryService.findAll());
    }
    @ApiOperation(value = "Gets list of ingredients", response = List.class)
    @GetMapping("/ingredients")
    public ResponseEntity<List<Ingredient>> findAllIngredients() throws ResourceNotFoundException{
        if (ingredientService.findAll().isEmpty()){
            throw new ResourceNotFoundException("No ingredients found");
        }
        return ResponseEntity.ok(ingredientService.findAll());
    }

    @GetMapping("{id}/ingredients")
    public ResponseEntity<List<PastryIngredient>> getPastryIngredients(@PathVariable Long id){
        return ResponseEntity.ok(pastryIngredientService.findByPastryId(id));
    }

    /*@GetMapping("/dingo")
    public ArrayList<IngredientStatistics> getStatistics(){
        return  pastryIngredientService.sumOfIngredient();
    }*/

    @ApiOperation(value = "Get a pastry by Id")
    @GetMapping("/{id}")
    public ResponseEntity<Pastry> getPastryById(
            @ApiParam(value = "Pastry id from which pastry object will retrieve", required = true)
            @Valid @PathVariable(value = "id") Long id) throws ResourceNotFoundException {

        Pastry pastry = pastryService.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Pastry not found for this id : " + id));

        return ResponseEntity.ok().body(pastry);
    }

    @ApiOperation(value = "Add a pastry")
    @PostMapping
    public ResponseEntity<?> create(
            @ApiParam(value = "Save pastry object in database", required = true)
            @Valid @RequestBody Pastry pastry, BindingResult result) {
        ResponseEntity<?> errorMap = mapValidationErrorService.MapValidationService(result);
        if(errorMap!=null) return errorMap;
        pastryService.addPastryIngredientsToPastry(pastry);
        return ResponseEntity.ok(pastryService.save(pastry));
    }

    @ApiOperation(value = "Get ingredient by id")
    @GetMapping("/ingredients/{id}")
    public ResponseEntity<Ingredient> getIngredients(@PathVariable Long id) throws ResourceNotFoundException{
        Optional<Ingredient> ingredientOptional = ingredientService.findById(id);
        if (ingredientOptional.isEmpty()) {
            throw new ResourceNotFoundException("Ingredients not found for this id: " + id);
        }
        return ResponseEntity.ok(ingredientOptional.get());
    }

    @ApiOperation(value = "Update pastry")
    @PutMapping("/{id}")
    public ResponseEntity<Pastry> update(
            @ApiParam(value = "Pastry Id to update pastry object", required = true)@PathVariable Long id,
            @ApiParam(value = "Update pastry object", required = true) @Valid @RequestBody Pastry pastry)
            throws ResourceNotFoundException{
        if(pastryService.findById(id).isEmpty()) {
            throw new ResourceNotFoundException("Pastry not found for this id: " + id);
        }
        pastryService.addPastryIngredientsToPastry(pastry); //adds pastry ingredients to specific pastry
        return ResponseEntity.ok(pastryService.save(pastry));
    }

    @ApiOperation(value = "Delete pastry by id")
    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(
            @ApiParam(value = "Pastry Id from which pastry object will delete from database table", required = true)
            @PathVariable Long id) throws ResourceNotFoundException{

        if (pastryService.findById(id).isEmpty()) {
            throw new ResourceNotFoundException("Pastry not found for this id: " + id);
        }
        pastryService.deleteById(id);
        return ResponseEntity.ok().build();
    }
}