package ee.ericerm.pastrybakery.repository;

import ee.ericerm.pastrybakery.model.Pastry;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PastryRepository extends JpaRepository<Pastry, Long> {
}
