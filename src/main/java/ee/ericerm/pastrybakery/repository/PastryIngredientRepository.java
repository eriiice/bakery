package ee.ericerm.pastrybakery.repository;

import ee.ericerm.pastrybakery.model.PastryIngredient;
import ee.ericerm.pastrybakery.service.IngredientStatistics;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public interface PastryIngredientRepository extends JpaRepository<PastryIngredient, Long> {

    List<PastryIngredient> findByPastryId(Long id);
/*
    @Query(value = "select ingredient_id, sum(amount) as total_amount " +
            "from pastry_ingredient " +
            "group by ingredient_id ", nativeQuery = true)
    ArrayList<IngredientStatistics> getSums();*/

    @Query(value = "SELECT ingredient.name, sum(pastry_ingredient.amount) as total_amount " +
            "from pastry_ingredient " +
            "inner join ingredient on pastry_ingredient.ingredient_id = ingredient.id " +
            "group by ingredient.name", nativeQuery = true)
    ArrayList<IngredientStatistics> sumOfIngredient();

   /* @Query("select new ee.ericerm.pastrybakery.service.IngredientStatistics(pi.ingredient_id as ingredientId, "
                    + "sum(pi.amount) as totalAmount, "
                    + "ingredient.name as name) "
                    + "from pastry_ingredient pi "
                    + "inner join pi "
                    + "on ingredient.id = pi.ingredient_id "
                    + "group by pi.ingredient_id")
    List<IngredientStatistics> groupBy();*/

}


