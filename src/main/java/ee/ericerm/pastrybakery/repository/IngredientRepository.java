package ee.ericerm.pastrybakery.repository;

import ee.ericerm.pastrybakery.model.Ingredient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public interface IngredientRepository extends JpaRepository<Ingredient, Long> {

    /*@Query(*//*value = "select pastry_ingredient.ingredient_id, ingredient.name, sum(pastry_ingredient.amount) as Total_amount from pastry_ingredient" +
            "INNER JOIN ingredient on pi.ingredient_id = ingredient.id" +
            "GROUP BY pi.ingredient_id, ingredient.name"*//*
    value = "select i.*, sum(pi.amount) as total_amount from ingredient i "+
            "inner join pastry_ingredient pi " +
                "on i.id = pi.ingredient_id " +
            "group by i.id, i.name, " +
            "order by total_amount desc ", nativeQuery = true)
    List<Ingredient> findSumForIngredientsById();*/
}
