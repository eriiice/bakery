package ee.ericerm.pastrybakery.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Data
@Entity
@NoArgsConstructor
@EqualsAndHashCode(exclude = "pastryIngredients")
@ApiModel(description = "All details about pastry.")
public class Pastry {

    @ApiModelProperty(notes = "The database generated pastry ID")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ApiModelProperty(notes = "The pastry name")
    @NotNull(message = "Name must be between 0 and 50 characters")
    @Size(max = 50)
    private String name;

    @ApiModelProperty(notes = "The relationship to ingredients")
    @OneToMany(mappedBy = "pastry", cascade = {CascadeType.MERGE, CascadeType.ALL})
    @JsonIgnoreProperties("pastry")
    @NotNull(message = "There have to be some ingredients")
    private Set<PastryIngredient> pastryIngredients;// = new HashSet<>();

    @NotNull(message = "Date can not be null")
    private Date created_at;

    @UpdateTimestamp
    private Date updated_at;

    public Pastry(String name){
        this.name = name;
    }

    public Pastry(@NotNull @Size(max = 100) String name, Date created_at, PastryIngredient... pastryIngredients) {
        this.name = name;
        this.created_at = created_at;
        for(PastryIngredient pastryIngredient : pastryIngredients)pastryIngredient.setPastry(this);
        this.pastryIngredients = Stream.of(pastryIngredients).collect(Collectors.toSet());
    }

    @Override
    public String toString() {
        return "";
    }
}
