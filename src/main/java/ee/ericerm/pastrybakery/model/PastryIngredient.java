package ee.ericerm.pastrybakery.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@Data
@Table(name = "pastry_ingredient")
@Entity
@ApiModel(description = "All details about pastry_ingredient joint table.")
public class PastryIngredient implements Serializable{

    @ApiModelProperty(notes = "The database generated pastry ID")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ApiModelProperty(notes = "The Pastry in joint table")
    @ManyToOne
    @JoinColumn(name = "pastry_id")
    @JsonIgnoreProperties("pastryIngredients")
    private Pastry pastry;

    @ApiModelProperty(notes = "The Ingredient in joint table")
    @ManyToOne
    @JoinColumn(name = "ingredient_id")
    @JsonIgnoreProperties("pastryIngredients")
    private Ingredient ingredient;

    @ApiModelProperty(notes = "The amount for ingredient")
    @NotNull(message = "Amount can not be null")
    @Column(name = "amount")
    private Long amount;

    public PastryIngredient(Pastry pastry, Ingredient ingredient, Long amount) {
        this.pastry = pastry;
        this.ingredient = ingredient;
        this.amount = amount;
    }
}