package ee.ericerm.pastrybakery.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

@Entity
@Data
@EqualsAndHashCode(exclude = "pastryIngredients")
@ApiModel(description = "All details about ingredient.")
public class Ingredient {

    @ApiModelProperty(notes = "The database generated ingredient ID")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ApiModelProperty(notes = "The ingredient name")
    @NotNull
    private String name;

    @ApiModelProperty(notes = "The One-To-Many relation to Pastry")
    @OneToMany(mappedBy = "ingredient", cascade = CascadeType.MERGE)
    @JsonIgnoreProperties("ingredient")
    @JsonIgnore
    private Set<PastryIngredient> pastryIngredients = new HashSet<>();

    public Ingredient() {
    }
    public Ingredient(String name){
        this.name = name;
    }
}
