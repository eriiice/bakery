package ee.ericerm.pastrybakery.service;

import ee.ericerm.pastrybakery.model.Pastry;
import ee.ericerm.pastrybakery.model.PastryIngredient;
import ee.ericerm.pastrybakery.repository.PastryRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class PastryService {
    private final PastryRepository pastryRepository;

    public List<Pastry> findAll(){
        return pastryRepository.findAll();
    }

    public Optional<Pastry> findById(Long id){
        return pastryRepository.findById(id);
    }

    public Pastry save(Pastry pastry){
        return pastryRepository.save(pastry);
    }

    public void deleteById(Long id){
        pastryRepository.deleteById(id);
    }

    public Pastry addPastryIngredientsToPastry(Pastry pastry){
        for (PastryIngredient pastryIngredient1:pastry.getPastryIngredients()){
            pastryIngredient1.setPastry(pastry);
        }
        return pastry;
    }
}
