package ee.ericerm.pastrybakery.service;

import ee.ericerm.pastrybakery.model.Ingredient;
import ee.ericerm.pastrybakery.repository.IngredientRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class IngredientService {
    private final IngredientRepository ingredientRepository;

    public List<Ingredient> findAll(){
        return ingredientRepository.findAll();
    }
    public Optional<Ingredient> findById(Long id){
        return ingredientRepository.findById(id);
    }
    /*public List<Ingredient> findSumForIngredientsById(){
        return ingredientRepository.findSumForIngredientsById();
    }*/
}
