package ee.ericerm.pastrybakery.service;

import ee.ericerm.pastrybakery.model.Ingredient;

public interface IngredientStatistics {
    Ingredient getIngredient();
    Long getAmount();
}
