package ee.ericerm.pastrybakery.service;

import ee.ericerm.pastrybakery.model.PastryIngredient;
import ee.ericerm.pastrybakery.repository.PastryIngredientRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class PastryIngredientService {
    private final PastryIngredientRepository pastryIngredientRepository;

    public List<PastryIngredient> findByPastryId(Long id){
        return pastryIngredientRepository.findByPastryId(id);
    }

    public ArrayList<IngredientStatistics> sumOfIngredient(){
     return pastryIngredientRepository.sumOfIngredient();
    }
}

