
package ee.ericerm.pastrybakery.bootstrap;

import ee.ericerm.pastrybakery.model.Ingredient;
import ee.ericerm.pastrybakery.repository.IngredientRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;

//Inserts Ingredients into table on boot

@Slf4j
@Component
@RequiredArgsConstructor
public class IngredientBootstrap implements ApplicationListener<ContextRefreshedEvent> {
    private final IngredientRepository ingredientRepository;

    @Override
    @Transactional
    public void onApplicationEvent(ContextRefreshedEvent event){
        if(ingredientRepository.count()==0L){
            log.debug("Loading ingredients");
            buildIngredient();

        }
    }
    private static Ingredient buildIngredient(String name){
        Ingredient ingredient = new Ingredient();
        ingredient.setName(name);
        return ingredient;
    }
    private void buildIngredient(){
        List<Ingredient> ingredientList = new LinkedList<>();
        ingredientList.add(buildIngredient("Muna"));
        ingredientList.add(buildIngredient("Piim"));
        ingredientList.add(buildIngredient("Jahu"));
        ingredientList.add(buildIngredient("Või"));
        ingredientList.add(buildIngredient("Suhkur"));
        ingredientList.add(buildIngredient("Pärm"));
        ingredientList.add(buildIngredient("Sool"));
        ingredientList.add(buildIngredient("Vesi"));

        ingredientRepository.saveAll(ingredientList);

    }
}

