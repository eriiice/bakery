# Bakery using Spring Boot + React
## Build and run
---
### Prerequisites
* JDK 1.8 or later
* Maven 3.2+


## DB PostgreSQL
---
Database connection configurable in ./src/main/resources/`application.properties`

*`spring.datasource.url=jdbc:postgresql://localhost:5432/bakery`

*`spring.datasource.username=postgres`

*`spring.datasource.password=1234`

`default:
	host: "localhost",
	database: "bakery",
	username: "postgres",
	password: "1234"`


## Available Scripts
---
### To start the front end:
In the project directory, you can run:
### `cd .\frontend\ ;  npm run start`

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### To start the Spring Boot

Run it with maven

## Comments
---
* Front-end in React not complete
* Made form for creating pastry with ingredients but no implementation of onSubmit
* Routes security
* Should add units for different ingredients
* Statistics not shown
* Would be good to add proper logging on back-end
* Using only one controller, might be better to use 2 separate ones (ingredient & pastry)
* Haven't done automated testing
* On a bigger scale, should use hypermedia in responses


## Bugs
---
* Posting in createpastry form does not work because pastryingredients can't be null
* Posting ingredients with only id-s gets response with ingredient name = null (but does not change it in table)


## API documentation
---
[http://localhost:8080/swagger-ui.html](http://localhost:8080/swagger-ui.html).

