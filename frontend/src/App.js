import React from 'react';
import './App.css';
import {Navbar, Nav} from "react-bootstrap";

//using it as a navbar
function App() {
  return (
      <Navbar bg="light" expand="sm">
        <Navbar.Brand href="/list">Bakery</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="mr-auto">
            <Nav.Link href="/statistics">Statistics</Nav.Link>
            <Nav.Link href="/list">List of Pastries</Nav.Link>
            <Nav.Link href="/create">Add Pastry</Nav.Link>
          </Nav>
        </Navbar.Collapse>
      </Navbar>
  );
}

export default App;
