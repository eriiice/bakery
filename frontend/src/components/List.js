import React, { useState, useEffect } from 'react';
import axios from 'axios';
import ListGroup from 'react-bootstrap/ListGroup';
import Spinner from 'react-bootstrap/Spinner';
import { withRouter } from 'react-router-dom';

function List(props) {
    const [data, setData] = useState([]);
    const [showLoading, setShowLoading] = useState(true);
    const apiUrl = "http://localhost:8080/api/pastries";

    useEffect(() => {
        const fetchData = async () => {
            const result = await axios(apiUrl);
            setData(result.data);
            //console.log(data);
            setShowLoading(false);
        };

        fetchData();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const showDetail = (id) => {
        props.history.push({
            pathname: '/show/' + id
        });
    };

    return (
        <div>
            {showLoading && <Spinner animation="border" role="status">
                <span className="sr-only">Loading...</span>
            </Spinner> }
            {/*<ListGroup>
                {data.map((item) => (
                    <ListGroup.Item key={item._links.self.href} action onClick={() => { showDetail(item.id) }}>{item.name}</ListGroup.Item>
                ))}
            </ListGroup>*/}
            <ListGroup>
                {data.map((item, idx) => (
                    <ListGroup.Item key={idx} action onClick={() => { showDetail(item.id) }}>{item.name}</ListGroup.Item>
                ))}
            </ListGroup>
        </div>
    );
}
export default withRouter(List);
