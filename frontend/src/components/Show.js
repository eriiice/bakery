import React, { useState, useEffect } from 'react';
import axios from 'axios';
import {Spinner, Jumbotron, Button} from 'react-bootstrap';
import { withRouter } from 'react-router-dom';
//import ListGroup from "react-bootstrap/ListGroup";

function Show(props) {
    const [data, setData] = useState([]);
    //const [ingredientsData, setIngredientsData] = useState({});
    const [showLoading, setShowLoading] = useState(true);
    const apiUrl = "http://localhost:8080/api/pastries/" + props.match.params.id;

    useEffect(() => {
        setShowLoading(false);
        const fetchData = async () => {
            const result = await axios(apiUrl);
            setData(result.data);
            setShowLoading(false);
        };
        //Gets ingredients for pastry with certain id

        /*const getIngredients = async () =>{
            const res = await axios("http://localhost:8080/api/pastries/ingredient/"+props.match.params.id);
            setIngredientsData(res.data);
        };
        getIngredients();*/
        fetchData();
        console.log(data)
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [apiUrl]);

    const editProduct = (id) => {
        props.history.push({
            pathname: '/edit/' + id
        });
    };

    const deleteProduct = (id) => {
        setShowLoading(true);
        const pastry = {
            name: data.name,
            created_at: data.created_at,
            updated_at: data.updated_at,
            pastryIngredients: data.pastryIngredients};
        axios.delete(apiUrl, pastry)
            .then((result) => {
                setShowLoading(false);
                props.history.push('/list')
            }).catch((error) => setShowLoading(false));
    };

    //console.log(JSON.stringify(data));
    return (
        <div>
            {showLoading && <Spinner animation="border" role="status">
                <span className="sr-only">Loading...</span>
            </Spinner> }
            <Jumbotron>
                <h1>{data.id}</h1>
                <h1>{data.name}</h1>
                <p>{data.created_at}</p>

                {/*<ListGroup>
                    {data.map((item, idx) => (
                        <ListGroup.Item key={idx}>{item.name}</ListGroup.Item>
                    ))}
                </ListGroup>*/}
                <p>
                    <Button type="button" variant="primary" onClick={() => { editProduct(data.id) }}>Edit</Button>&nbsp;
                    <Button type="button" variant="danger" onClick={() => { deleteProduct(data.id) }}>Delete</Button>
                </p>
            </Jumbotron>
        </div>
    );
}

export default withRouter(Show);
