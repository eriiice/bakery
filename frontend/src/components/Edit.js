import React, { useState, useEffect } from 'react';
import axios from 'axios';
import {Spinner, Jumbotron, Form, Button} from 'react-bootstrap';
import { withRouter } from 'react-router-dom';

function Edit(props) {

    const [pastry, setPastry] = useState({ id: '', name: '', created_at: ''});
    const [showLoading, setShowLoading] = useState(true);
    const apiUrl = "http://localhost:8080/api/pastries/" + props.match.params.id;

    useEffect(() => {
        setShowLoading(false);
        const fetchData = async () => {
            const result = await axios(apiUrl);
            setPastry(result.data);
            console.log(result.data);
            setShowLoading(false);
        };

        fetchData();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const updateProduct = (e) => {
        setShowLoading(true);
        e.preventDefault();
        const data = { id: pastry.id, name: pastry.name, created_at: pastry.created_at };
        axios.put(apiUrl, data)
            .then((result) => {
                setShowLoading(false);
                props.history.push('/show/' + result.data.id)
            }).catch((error) => setShowLoading(false));
    };
    const onChange = (e) => {
        e.persist();
        setPastry({...pastry, [e.target.name]: e.target.value});
    };

    return (
        <div>
            {showLoading &&
            <Spinner animation="border" role="status">
                <span className="sr-only">Loading...</span>
            </Spinner>
            }
            <Jumbotron>
                <Form onSubmit={updateProduct}>
                    <Form.Group>
                        <Form.Label>Pastry Name</Form.Label>
                        <Form.Control type="text" name="name" id="name" placeholder="Enter pastry name" value={pastry.name} onChange={onChange} required/>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Pastry Date</Form.Label>
                        <Form.Control as="textarea" name="created_at" id="created_at" rows="3" placeholder="Enter created at" value={pastry.created_at} onChange={onChange} required/>
                    </Form.Group>

                    {/* Missing ingredient showing */}

                    <Button variant="primary" type="submit">
                        Update
                    </Button>
                </Form>
            </Jumbotron>
        </div>
    );
}
export default withRouter(Edit)
