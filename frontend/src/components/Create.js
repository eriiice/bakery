import React, {useEffect, useState} from 'react';
import axios from 'axios';
import {Spinner, Jumbotron, Form, Button} from 'react-bootstrap';
import {withRouter} from 'react-router-dom';
import DatePicker from 'react-datepicker';
import "react-datepicker/dist/react-datepicker.css";
import Ingredient from "./Ingredient";


function Create(props) {
    const [pastry, setPastry] = useState({id: '', name: '', created_at: ''});
    const [showLoading, setShowLoading] = useState(false);
    const [pastryIngredients, setIngredients] = useState([]);
    const [startDate, setStartDate] = useState(new Date());
    const blankIngredient = {id: '', name: ''};
    const [ingredient, setIngredient] = useState([{...blankIngredient}]);

    const apiUrl = "http://localhost:8080/api/pastries";

    useEffect(() => {
        const getIngredients = async () => {
            const res = await axios("http://localhost:8080/api/pastries/ingredients/");
            setIngredients(res.data);
        };
        getIngredients()
    }, []);

    const saveProduct = (e) => {
        setShowLoading(true);
        e.preventDefault();
        const data = {name: pastry.name, created_at: startDate, pastryIngredients: pastry.pastryIngredients};
        axios.post(apiUrl, data)
            .then((result) => {
                setShowLoading(false);
                props.history.push('/show/' + parseInt(result.data.id))
            }).catch((error) => setShowLoading(false));
    };

    /*const addIngredients = () => {
        setIngredient([...ingredient, {...blankIngredient}]);
    };*/
    /*const removeIngredients = idx => ()=>{
        setIngredient([...ingredient, {...blankIngredient}]);
        if(idx !==-1){
            ingredient.splice(idx, 1);
            setIngredient
        }
    };*/

    const handleIngredientChange = (e) => {
        const updatedIngredients = [...ingredient];
        updatedIngredients[e.target.dataset.idx][e.target.className] = e.target.value;
        setIngredient(updatedIngredients);
    };

    const onChange = (e) => {
        e.persist();
        setPastry({...pastry, [e.target.name]: e.target.value});
    };

    return (
        <div>
            {showLoading &&
            <Spinner animation="border" role="status">
                <span className="sr-only">Loading...</span>
            </Spinner>
            }
            <Jumbotron>
                <Form onSubmit={saveProduct}>
                    <Form.Group>
                        <Form.Label>Pastry Name</Form.Label>
                        <Form.Control type="text"
                                      name="name"
                                      id="name"
                                      placeholder="Enter pastry name"
                                      value={pastry.name}
                                      onChange={onChange} required/>
                    </Form.Group>

                    <Form.Label>Ingredients: </Form.Label>
                    <Form.Group>
                        {
                            ingredient.map((val, idx) => (
                                <Ingredient key={`ingredient-${idx}`}
                                            idx={idx}
                                            ingredient={ingredient}
                                            handleIngredientChange={handleIngredientChange}
                                            pastryIngredients={pastryIngredients}/>))
                        }
                    </Form.Group>

                    <Form.Label>Date</Form.Label>
                    <Form.Group>
                        <DatePicker dateFormat="dd/MM/yyyy"
                                    selected={startDate}
                                    onChange={date => setStartDate(date)}
                                    required/>
                    </Form.Group>
                    <Button variant="primary" type="submit">
                        Save
                    </Button>
                </Form>
            </Jumbotron>
        </div>
    );
}

export default withRouter(Create);
