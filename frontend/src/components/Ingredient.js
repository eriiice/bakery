import React, {useState} from "react";
import useForm from "react-hook-form";
import {Form, Button, Row, Col} from "react-bootstrap";

function Ingredient({ingredient, pastryIngredients}) {
    const [indexes, setIndexes] = useState([]);
    const [counter, setCounter] = useState(0);
    const {register, handleSubmit} = useForm();

    const onSubmit = data => {
        console.log(data);
    };

    const addIngredient = () => {
        setIndexes(prevIndexes => [...prevIndexes, counter]);
        setCounter(prevCounter => prevCounter + 1);
    };

    const removeIngredient = index => () => {
        setIndexes(prevIndexes => [...prevIndexes.filter(item => item !== index)]);
        setCounter(prevCounter => prevCounter - 1);
    };

    const clearIngredient = () => {
        setIndexes([]);
    };

    return (
        <form onSubmit={handleSubmit(onSubmit)}>
            {indexes.map(index => {
                const fieldName = `Ingredient[${index}]`;
                return (
                    <div name={fieldName} key={fieldName}>
                        <Row>
                            <Col>
                                <Form.Group>
                                    <Form.Control as="select" name={pastryIngredients} id={ingredient}>
                                        {
                                            pastryIngredients.map(ingredient => (
                                                <option key={ingredient.id}
                                                        value={ingredient.id}
                                                        ref={register}
                                                        name={`${fieldName}`}>
                                                    {ingredient.name}</option>
                                            ))
                                        }
                                    </Form.Control>
                                </Form.Group>
                            </Col>
                            <Col>
                                <Form.Group>
                                    <Form.Control
                                        type="number"
                                        name={`${fieldName}.amount`}
                                        ref={register}
                                        min="0"
                                        placeholder="Enter ingredient quantity"
                                    />
                                </Form.Group>
                            </Col>
                            <Col>
                                <Form.Group>
                                    <Button variant="danger" onClick={removeIngredient(index)}>
                                        Remove
                                    </Button>
                                </Form.Group>
                            </Col>
                        </Row>
                    </div>
                );
            })}
            <Form.Group>
                <Row>
                    <Col>
                        <Button onClick={addIngredient}>
                            Add ingredient
                        </Button>
                    </Col>
                    <Col>
                        <Button variant="danger" onClick={clearIngredient}>
                            Clear ingredients
                        </Button>
                    </Col>
                </Row>
            </Form.Group>
        </form>
    )
}

export default Ingredient;
