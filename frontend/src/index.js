import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import List from './components/List';
import Edit from './components/Edit';
import Create from './components/Create';
import Show from './components/Show';
import {BrowserRouter as Router, Route} from "react-router-dom";

ReactDOM.render(
    <Router>
    <div>
        <Route render ={()=> < App /> } path="/" />
        <Route render ={()=> < List />} path="/list" />
        <Route render ={()=> < Edit />} path="/edit/:id" />
        <Route render ={()=> < Create />} path="/create" />
        <Route render ={()=> < Show />} path="/show/:id" />
    </div>
</Router>, document.getElementById('root'));

serviceWorker.unregister();
